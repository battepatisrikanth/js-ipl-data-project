const csvMatchesFilePath = 'src/data/matches.csv';
const fs = require('fs')
const csvtojson=require('csvtojson')

csvtojson()
.fromFile(csvMatchesFilePath)
.then((jsonObject)=>{
    const teams =[];
    for(let index in jsonObject){
        if(jsonObject[index].toss_winner == jsonObject[index].winner){
            teams.push(jsonObject[index].winner);
        }
    }
    let tossAndWinPerTeam = {};
    teams.forEach((team) => { tossAndWinPerTeam[team] = (tossAndWinPerTeam[team] || 0) + 1; });
    console.log(tossAndWinPerTeam);
    fs.writeFileSync('src/public/output/6-number-of-times-won-toss-and-match-per-team.json',JSON.stringify(tossAndWinPerTeam)); 
})
    