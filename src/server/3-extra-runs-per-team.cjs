const csvDeliveriesFilePath='src/data/deliveries.csv';
const csvMatchesFilePath = 'src/data/matches.csv';
const csvtojson=require('csvtojson');
const fs = require('fs');
let matchIds = [];
csvtojson()
.fromFile(csvMatchesFilePath)
.then((jsonObject)=>{
    for(let index in jsonObject){
        if(jsonObject[index].season == 2016){
            matchIds.push(jsonObject[index].id);
        }
    }
})
csvtojson().fromFile(csvDeliveriesFilePath).then((jsonObject) => {
    let ExtraRunsPerTeam = [];
    for(let index in jsonObject){
        if(matchIds.includes(jsonObject[index].match_id)){
            let found = false;
            for(let innerIndex in ExtraRunsPerTeam){
                if(ExtraRunsPerTeam[innerIndex].team == jsonObject[index].bowling_team){
                    ExtraRunsPerTeam[innerIndex].ExtraRuns = parseInt(ExtraRunsPerTeam[innerIndex].ExtraRuns) + parseInt(jsonObject[index].extra_runs);
                    found = true;
                }
            }
            if(!found){
                ExtraRunsPerTeam.push(new Extraruns(jsonObject[index].bowling_team,jsonObject[index].extra_runs ))
            }
        }
    }
    console.log(ExtraRunsPerTeam);
    fs.writeFileSync('src/public/output/3-extra-runs-per-team.json',JSON.stringify(ExtraRunsPerTeam)); 
})

function Extraruns( team, ExtraRuns){
    this.team = team;
    this.ExtraRuns = ExtraRuns;
}
