const csvDeliveriesFilePath='src/data/deliveries.csv';
const csvMatchesFilePath = 'src/data/matches.csv';
const csvtojson=require('csvtojson')
const fs = require('fs')
let matchIds = [];
csvtojson()
.fromFile(csvMatchesFilePath)
.then((jsonObject)=>{
    for(let index in jsonObject){
        if(jsonObject[index].season == 2015){
            matchIds.push(jsonObject[index].id);
        }
    }
})
csvtojson()
.fromFile(csvDeliveriesFilePath)
.then((jsonObject)=>{
    let bowlers =[];
    for(let index in jsonObject){
        if(matchIds.includes(jsonObject[index].match_id)){
            let found = false;
            for(let bowlerIndex in bowlers){
                if(bowlers[bowlerIndex].name == jsonObject[index].bowler){
                    if(jsonObject[index].wide_runs == '0' && jsonObject[index].noball_runs == '0'){
                        bowlers[bowlerIndex].ballsBowled++;
                    }
                    bowlers[bowlerIndex].runsConceded = parseInt(bowlers[bowlerIndex].runsConceded) + parseInt(jsonObject[index].total_runs) - parseInt(jsonObject[index].legbye_runs) -parseInt(jsonObject[index].bye_runs) -parseInt(jsonObject[index].penalty_runs);
                    found = true;
                }
            }
            if(!found){    
                let runsConceded = parseInt(jsonObject[index].total_runs) - parseInt(jsonObject[index].legbye_runs) -parseInt(jsonObject[index].bye_runs) -parseInt(jsonObject[index].penalty_runs);
                let ballsBowled = 0
                if(jsonObject[index].wide_runs == '0' && jsonObject[index].noball_runs == '0'){
                    ballsBowled++;
                }
                bowlers.push(new bowlerStats(jsonObject[index].bowler, runsConceded , ballsBowled))

            }
        }
    }
    for(let index in bowlers){
        bowlers[index].economy = parseFloat(bowlers[index].runsConceded/(bowlers[index].ballsBowled/6)).toFixed(2);
    }
    bowlers.sort(compare);
    let top10Bowlers = bowlers.slice(0,10);
    console.log(top10Bowlers);


    fs.writeFileSync('src/public/output/4-top10-economical-bowlers.json',JSON.stringify(top10Bowlers));
     
})

function compare( firstItem, secondItem ) {
    if ( parseFloat(firstItem.economy) < parseFloat(secondItem.economy) ){
      return -1;
    }
    if ( parseFloat(firstItem.economy) > parseFloat(secondItem.economy) ){
      return 1;
    }
    return 0;
  }
  

function bowlerStats(name, runsConceded, ballsBowled){
    this.name = name;
    this.runsConceded = runsConceded;
    this.ballsBowled = ballsBowled;
    this.economy = 0;
}
