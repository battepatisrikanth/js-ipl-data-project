const csvFilePath= 'src/data/matches.csv'
const csvtojson=require('csvtojson');
const fs = require('fs');
csvtojson()
.fromFile(csvFilePath)
.then((jsonObject)=>{
    const winners = [];
    for(let index in jsonObject){
        let winner = jsonObject[index].winner;
        let year = jsonObject[index].season;
        let found =false;
        for(let winnerIndex in winners){
            if(winners[winnerIndex].year==year && winners[winnerIndex].team==winner){
                winners[winnerIndex].wins += 1;
                found = true;
                break;
            }
        }
        if(!found){
            winners.push(new entry(jsonObject[index].season,jsonObject[index].winner));
        }
    }
    fs.writeFileSync("src/public/output/2-matches-won-per-team-per-year.json", JSON.stringify(winners));
    console.log(winners);

})

class entry {
    constructor(year, team) {
        this.year = year;
        this.team = team;
        this.wins = 1;
    }
}
