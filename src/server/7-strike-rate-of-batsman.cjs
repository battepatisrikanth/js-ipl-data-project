const csvDeliveriesFilePath='src/data/deliveries.csv';
const csvMatchesFilePath = 'src/data/matches.csv';
const fs = require('fs')
const csvtojson=require('csvtojson')
let matchesPerYear = {};
csvtojson()
.fromFile(csvMatchesFilePath)
.then((jsonObject)=>{ 
    for(let index in jsonObject){
        let year = jsonObject[index].season;
        if(matchesPerYear[jsonObject[index].season] == undefined){
            matchesPerYear[jsonObject[index].season] = [];
        }
        matchesPerYear[year].push(jsonObject[index].id);
    }
})
csvtojson()
.fromFile(csvDeliveriesFilePath)
.then((jsonObject)=>{
    let batsmen = []; 
    for(let index in jsonObject){
        let matchId = jsonObject[index].match_id;
        let year = findYear(matchId);
        let found = false;
        for(batsmanIndex in batsmen){
            if(batsmen[batsmanIndex].year == year && batsmen[batsmanIndex].name == jsonObject[index].batsman){
                batsmen[batsmanIndex].totalRuns = parseInt(batsmen[batsmanIndex].totalRuns)+ parseInt(jsonObject[index].batsman_runs);
                if(jsonObject[index].wide_runs == '0' && jsonObject[index].noball_runs == '0'){
                    batsmen[batsmanIndex].totalBalls += 1;
                }
                found = true;
                break;
            }
        }
        if(!found){
            let ballsBowled  = 0;
            if(jsonObject[index].wide_runs == '0' && jsonObject[index].noball_runs == '0'){
                ballsBowled = 1;
            }
            batsmen.push(new batsman(year,jsonObject[index].batsman, jsonObject[index].batsman_runs, ballsBowled));
        }
    }
    calculateStrikeRate(batsmen);
    console.log(batsmen);
    fs.writeFileSync('src/public/output/7-strike-rate-of-batsman.json',JSON.stringify(batsmen)); 
})

function findYear(matchId){
    for(let index in matchesPerYear){
        for(let innerIndex in matchesPerYear[index]){
            if(matchId==matchesPerYear[index][innerIndex])
            return index;
        }
        
    }
}

function calculateStrikeRate(batsmen){
    for(index in batsmen){
        batsmen[index].strikeRate = ((batsmen[index].totalRuns/batsmen[index].totalBalls)*100).toFixed(2);
    }
}

function batsman(year,name, totalRuns, totalBalls){
    this.year = year;
    this.name = name;
    this.totalRuns= totalRuns;
    this.totalBalls= totalBalls;
    this.strikeRate = 0;
}
