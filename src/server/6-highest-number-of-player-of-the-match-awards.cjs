const csvMatchesFilePath = 'src/data/matches.csv';
const fs = require('fs')
const csvtojson=require('csvtojson')
csvtojson()
.fromFile(csvMatchesFilePath)
.then((jsonObject)=>{
    let players = [];
    for(let index in jsonObject){
        let found = false;
        for(let playerIndex in players){
            if(jsonObject[index].player_of_match == players[playerIndex].name && jsonObject[index].season == players[playerIndex].year){
                players[playerIndex].potm +=1
                found =true;
            }
        }
        if(!found){
            players.push(new player(jsonObject[index].season, jsonObject[index].player_of_match, 1));
        }
    }
    players.sort(compare);
    let playerOfTheYear =[];
    for(let index in players){
        let found = false;
        for(let innerIndex in playerOfTheYear){
            if(players[index].year == playerOfTheYear[innerIndex].year){
                found = true;
            }
        }
        if(!found){
            playerOfTheYear.push(players[index])
        }
    }
    console.log(playerOfTheYear);
    fs.writeFileSync('src/public/output/5-highest-number-of-player-of-the-match-awards.json',JSON.stringify(playerOfTheYear)); 
})
function compare( firstItem, secondItem ) {
    if ( parseInt(firstItem.year) < parseInt(secondItem.year) ){
      return 1;
    }
    else if ( parseInt(firstItem.year) > parseInt(secondItem.year) ){
      return -1;
    }
    else if(parseInt(firstItem.potm) < parseInt(secondItem.potm)){
        return 1;
    }
    else if(parseInt(firstItem.potm) > parseInt(secondItem.potm)){
        return -1;
    }
  }

function player(year, name, potm ){
    this.year = year;
    this.name = name;
    this.potm = potm;
}
