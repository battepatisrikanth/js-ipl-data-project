const csvDeliveriesFilePath='src/data/deliveries.csv';
const csvMatchesFilePath = 'src/data/matches.csv';
const fs = require('fs')
const csvtojson=require('csvtojson')
csvtojson()
.fromFile(csvDeliveriesFilePath)
.then((jsonObject)=>{
    let wickets = []
    for(let index in jsonObject){
        if(jsonObject[index].player_dismissed != ''){
            let found = false;
            for(let wicketIndex in wickets){
                if(wickets[wicketIndex].bowler==jsonObject[index].bowler && wickets[wicketIndex].dissmisedPlayer==jsonObject[index].player_dismissed){
                    wickets[wicketIndex].times += 1;
                    found=true;
                    break;
                }
            }
            if(!found){
                wickets.push(new wicket(jsonObject[index].bowler,jsonObject[index].player_dismissed));
            }
        }
    }
    wickets.sort(compare);
    console.log(wickets);
    fs.writeFileSync('src/public/output/8-highest-number-of-times-dismissed-by-another-palyer.json',JSON.stringify(wickets[0])); 
})

function compare( firstItem, secondItem){
    if(firstItem.times > secondItem.times){
        return -1;
    }
    else if(firstItem.times < secondItem.times){
        return 1;
    }
}

function wicket(bowler,dissmisedPlayer){
    this.bowler = bowler;
    this.dissmisedPlayer = dissmisedPlayer;
    this.times = 1;
}
