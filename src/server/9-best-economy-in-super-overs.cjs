const csvDeliveriesFilePath='src/data/deliveries.csv';
const fs = require('fs')
const csvtojson=require('csvtojson')
csvtojson()
.fromFile(csvDeliveriesFilePath)
.then((jsonObject)=>{
    let bowlers = [];
    for(let index in jsonObject){
        if(jsonObject[index].is_super_over != '0'){
            let found = false;
            for(bowlerIndex in bowlers){
                if(jsonObject[index].bowler == bowlers[bowlerIndex].name)
                {
                    let runsConceded = parseInt(jsonObject[index].total_runs) - parseInt(jsonObject[index].bye_runs) - parseInt(jsonObject[index].legbye_runs) - parseInt(jsonObject[index].penalty_runs);
                    bowlers[bowlerIndex].runsConceded += runsConceded;
                    if(jsonObject[index].wide_runs == '0' && jsonObject[index].noball_runs == '0'){
                        bowlers[bowlerIndex].ballsBowled++;
                    }
                    found = true;
                }
            }
            if(found == undefined){
                let runsConceded = parseInt(jsonObject[index].total_runs) - parseInt(jsonObject[index].bye_runs) - parseInt(jsonObject[index].legbye_runs) - parseInt(jsonObject[index].penalty_runs);
                let ballsBowled  = 0;
                if(jsonObject[index].wide_runs == '0' && jsonObject[index].noball_runs == '0'){
                    ballsBowled = 1;
                }
                bowlers.push(new bowler(jsonObject[index].bowler));
            }  
        }
    }
    calculateEconomy(bowlers);
    bowlers.sort(compare);
    console.log(bowlers);
    fs.writeFileSync('src/public/output/9-best-economy-in-super-overs.json',JSON.stringify(bowlers[0])); 
})

function bowler(name, runsConceded, ballsBowled){
    this.name = name;
    this.runsConceded = runsConceded;
    this.ballsBowled = ballsBowled;
    this.economy = 0;
}

function calculateEconomy(bowlers){
    for(index in bowlers){
        bowlers[index].economy = Number((parseFloat(bowlers[index].runsConceded)/(parseFloat(bowlers[index].ballsBowled)/6)).toFixed(2));
    }
    return bowlers;
}

function compare( firstItem,secondItem){
    if(firstItem.economy < secondItem.economy)
    return -1;
    if(firstItem.economy > secondItem.economy)
    return 1;
}
